all: build run

build: bib-o-mat2000.bas
	petcat -w2 -o bib-o-mat2000.prg -- bib-o-mat2000.bas
	c1541 -format bib-o-mat2000,id d64 bib-o-mat2000.d64 -attach bib-o-mat2000.d64 -write bib-o-mat2000.prg bib-o-mat2000

run: bib-o-mat2000.d64
	x64 bib-o-mat2000.d64
