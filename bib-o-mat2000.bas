10 rem indexed filing system
20 rem version 1
30 open 15,8,15
40 gosub 14000:print#15,"i":gosub 5000:print:print
50 print"(1) create new file"
60 print"(2) access existing file"
70 print"(3) exit program"
80 print:print
90 input "select option";s%
100 if s%<1 or s%>3 then 40
110 on s% gosub 2000,1000,280
120 print chr$(147)
130 open 6,8,6,"0:"+n$:gosub 5000
140 gosub 14000:print:print
150 print "(1) enter a record"
160 print "(2) display a record"
170 print "(3) delete a record"
180 print "(4) modify a field"
190 print "(5) sort index"
200 print "(6) display file"
210 print "(7) check index bytes free"
220 print "(8) end program : save index"
230 print:print:input "select option";s%
240 if s%<1 or s%>8 then 140
250 on s% gosub 6000,7000,8000,9000,10000,11000,23000,270
260 goto 140
270 close 6:gosub 3000:close 15
280 end
997 rem *
998 rem * *
999 rem load index subroutine
1000 print chr$(147):gosub 4000
1010 open 5,8,5,"0:i."+n$+",s,r"
1020 gosub 5000:print chr$(147):print "reading heading/index file"
1030 input#5,nf%,fs%
1040 dim i$(1,fs%)
1050 for f=1 to nf%
1060 input#5,h$(f),w(f),p(f)
1070 next
1080 input#5,l%
1090 for r=1 to l%-1
1100 for c=0 to 1
1110 input#5,i$(c,r)
1120 next:next
1130 close 5
1140 return
1997 rem *
1998 rem * *
1999 rem create file subroutine
2000 print chr$(147):gosub 4000
2010 print "enter file size (number of records)"
2020 input fs%
2030 if fs%<1 then 2010
2040 print "enter number of fields required"
2050 input nf%
2060 if nf%<2 or nf%>9 then print "fields (2-9) only":goto 2040
2070 dim i$(1,fs%):p(1)=1:l%=1
2080 for f=1 to nf%
2090 print chr$(147)
2100 print "enter field heading";f
2110 w=255:gosub 22000:h$(f)=k$
2120 print "enter field width (characters)"
2130 input w(f)
2140 if w(f)<1 then 2120
2150 p(f+1)=p(f)+w(f)+2
2160 next
2170 if p(nf%+1) > 254 then print "too long : start again":gosub 15000:goto 2070
2180 print chr$(147):print "creating main file ";n$
2190 open 6,8,6,"0:"+n$+",l,"+chr$(p(nf%+1))
2200 gosub 5000
2210 r%=r:gosub 12000:gosub 13000
2220 print#6,"end"
2230 close 6
2240 open 6,8,6,"0:"+n$
2250 gosub 6000
2260 close 6
2270 return
2997 rem *
2998 rem * *
2999 rem save index subroutine
3000 print chr$(147)
3010 open 5,8,5,"@0:i."+n$+",s,w"
3020 gosub 5000:print "writing heading/index file"
3030 print#5,nf%:print#5,fs%
3040 for f=1 to nf%
3050 print#5,h$(f):print#5,w(f):print#5,p(f)
3060 next
3070 print#5,l%
3080 gosub 5000
3090 for r=1 to l%-1
3100 for c=0 to 1
3110 print#5,i$(c,r)
3120 next:next
3130 close 5
3140 return
3997 rem *
3998 rem * *
3999 rem get file name subroutine
4000 print "enter file name"
4010 w=14:gosub 22000:n$=k$
4020 return
4997 rem *
4998 rem * *
4999 rem rem read error channel subroutine
5000 input#15,a,b$,c,d
5020 if a<20 or a=50 then 5040
5030 print chr$(147):print b$:gosub 15000:run
5040 return
5997 rem *
5998 rem * *
5999 rem enter a record subroutine
6000 print chr$(147)
6010 print"enter a record"
6020 print:print
6030 for f=1 to nf%
6040 print "maximum characters";w(f)
6050 print "enter ";h$(f)
6060 w=w(f):gosub 22000:f$(f)=k$
6070 next
6080 gosub 16000
6090 return
6997 rem *
6998 rem * *
6999 rem retrieve record subroutine
7000 print chr$(147):print "enter key field"
7010 fl%=0
7020 w=255:gosub 22000:ky$=k$
7030 print chr$(147):print "retrieving record"
7040 j%=1
7060 if ky$=i$(0,j%) then r%=val(i$(1,j%)):gosub 19000:gosub 20000: goto 7100
7070 j%=j%+1
7080 if j%>fs% then print "record not on file":fl%=1:gosub 15000:goto 7100
7090 goto 7060
7100 return

10997 rem *
10998 rem * *
10999 rem display file subroutine
11000 j%=0
11010 j%=j%+1
11020 r%=val(i$(1,j%))
11030 gosub 19000
11040 gosub 20000
11050 print:print
11060 print "press space bar to step through file"
11070 print "press any other key to quit"
11080 get k$
11090 if k$="" then 11080
11100 if j%=l%-1 then 11120
11110 if k$=chr$(32) then 11010
11120 return
11997 rem *
11998 rem * *
11999 rem calculate file pointer
12000 hb%=r%/256
12010 lb%=r%-hb%*256
12020 return
12997 rem *
12998 rem * *
12999 rem set file pointer
13000 print#15,"p"chr$(6)chr$(lb%)chr$(hb%)chr$(p(f))
13010 return
13997 rem *
13998 rem * *
13999 rem title subroutine
14000 print chr$(147)
14010 print "bib-o-mat2000"
14020 return
14997 rem *
14998 rem * *
14999 rem wait for any key pressed
15000 print:print "press any key to continue"
15010 get k$
15020 if k$="" then 15010
15030 return
15997 rem *
15998 rem * *
15999 rem store record subroutine
16000 print chr$(147)
16010 gosub 17000
16020 if t%=1 then print "rejected: key field not unique":gosub 15000:goto 16060
16030 if l%>fs% and t%=0 then print "rejected: file full":gosub 15000:goto 16060
16040 if t%=0 then r%=l%:gosub 21000:gosub 18000
16050 if t%=-1 then i$(0,i%)=f$(1):r%=val(i$(1,i%)):gosub 21000
16060 return
16997 rem *
16998 rem * *
16999 rem check key (unique/tombstone)
17000 t%=0:j%=0
17010 j%=j%+1
17020 if f$(1)=i$(0,j%) then t%=1:goto 17050
17030 if i$(0,j%)=chr$(97) then T%=-1:i%=j%
17040 if j%<l%-1 then 17010
17050 return
17997 rem *
17998 rem * *
17999 rem update index subroutine
18000 i$(0,l%)=f$(1)
18010 i$(1,l%)=str$(l%)
18020 l%=l%+1
18030 return
18997 rem *
18998 rem * *
18999 rem read record subroutine
19000 gosub 12000
19010 for f=1 to nf%
19020 gosub 13000
19030 input#6,f$(f)
19040 next
19050 return
19997 rem *
19998 rem * *
19999 rem display record subroutine
20000 print chr$(147):print "file accessed:";n$
20010 print "display of record number";r%
20020 print:print
20030 for f=1 to nf%
20040 print h$(f),f$(f)
20050 next
20060 if s%=2 then gosub 15000
20070 return
20997 rem *
20998 rem * *
20999 rem write record subroutine
21000 if r%<l% then gosub 24000
21010 gosub 12000
21020 for f=1 to nf%
21030 gosub 13000
21040 print#6,f$(f)
21050 next
21060 if r%<l% then gosub 24000
21070 return
21997 rem *
21998 rem * *
21999 rem input validation subroutine
22000 k$="":input k$
22010 if k$="" then 22000
22020 if len(k$)>w then k$=left$(k$,w)
22030 return
22997 rem *
22998 rem * *
22999 rem bytes free subroutine
23000 print chr$(147):print"wait":print
23010 x=fre(0)-(sgn(fre(0))<0)*65535
23020 print "number of index bytes free:";x
23030 gosub 15000
23040 return
23997 rem *
23998 rem * *
23999 rem clear out buffer to disc
24000 close 6
24010 open 6,8,6,"0:"+n$
24020 return
